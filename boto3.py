 # Lets create EC2 instances using Python BOTO3
from datetime import datetime, timedelta

import boto3
import click
import fabric
import pytz

#paramico библиотека почитать

def create_ec2_instance():

    try:
        print ("Creating EC2 instance")
        resource_ec2 = boto3.client("ec2")
        ec2 = boto3.resource('ec2')
        cloudwatch = boto3.client('cloudwatch')

        #response = resource_ec2.delete_key_pair(KeyName='labs')
        #response2 = resource_ec2.create_key_pair(KeyName='labs')


        #file = open("/Users/vladimir/labs.pem", "w")
        #file.write(response2['KeyMaterial'])
        #file.close()

        #print (response2['KeyMaterial'])

        resource_ec2.run_instances(
            ImageId="ami-0574da719dca65348",
            MinCount=1,
            MaxCount=1,
            InstanceType="t2.micro",
            KeyName="test",
            TagSpecifications=[
                {
                    'ResourceType': 'instance',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ]
                },
            ],
        )

        response = resource_ec2.describe_instances(
            Filters=[
                {
                    'Name': 'tag:string',
                    'Values': [
                        'string',
                    ],
                    'Name': 'instance-state-name',
                    'Values': [
                        'pending',
                    ],
                },
            ],
        )

        val = resource_ec2.describe_instance_types()
        #describe_id_format()
        #val = resource_ec2.describe_key_pairs()
        #print (val)
        print (val["InstanceTypes"][0]["InstanceType"])
        print (val["InstanceTypes"][0]["MemoryInfo"])

        #print (response)
        #for ec in response:
           # print (ec.)

        print (response["Reservations"][0]["Instances"][0]["InstanceId"])
        print (response["Reservations"][0]["Instances"][0]["PrivateIpAddress"])
        print (response["Reservations"][0]["Instances"][0]["PlatformDetails"])
        print (response["Reservations"][0]["Instances"][0]["InstanceType"])

        waiter = resource_ec2.get_waiter('instance_status_ok')

        ip = response["Reservations"][0]["Instances"][0]["PublicIpAddress"]

        print (ip)

        click.pause('Чтобы продолжить, нажмите Enter.')



        waiter.wait(
            InstanceIds=[
                response["Reservations"][0]["Instances"][0]["InstanceId"],
            ]
        )



        c = fabric.Connection(
            host=ip,
            user="ubuntu",
            connect_kwargs={
                "key_filename": "/Users/vladimir/test.pem",
            },
        )

        c.put('/Users/vladimir/.ssh/id_rsa.pub', '/home/ubuntu')
        c.run('rm /home/ubuntu/.ssh/authorized_keys')
        c.run('mv /home/ubuntu/id_rsa.pub /home/ubuntu/.ssh/authorized_keys')

        utc_now = datetime.now(tz=pytz.utc)
        response1 = cloudwatch.get_metric_data(
            MetricDataQueries=[
                {
                    "Id": "result1",
                    "MetricStat": {
                        "Metric": {
                            "Namespace": "AWS/EC2",
                            "MetricName": "CPUUtilization",
                            "Dimensions": [{"Name": "InstanceId", "Value": response["Reservations"][0]["Instances"][0]["InstanceId"]}],
                        },
                        "Period": 60,
                        "Stat": "Sum",
                    },
                },
            ],
            StartTime=utc_now - timedelta(seconds=60),
            EndTime=utc_now + timedelta(seconds=60),
        )

        # len(response1["MetricDataResults"]).should.equal(1)
        res1 = response1["MetricDataResults"][0]
        print (response1["MetricDataResults"][0]["Label"])
        print (response1["MetricDataResults"][0]["Values"])





        #paginator = cloudwatch.get_paginator('list_metrics')
        #or response in paginator.paginate(Dimensions=[{'Name': 'InstanceId', 'Value': response["Reservations"][0]["Instances"][0]["InstanceId"]}],
          #                                 Namespace='AWS/EC2'):
         #   for i in response['Metrics']:
                #print(response["Metrics"][i]["MetricName"] + ':')
                #print(response["Metrics"][i]["Dimensions"])
          #      print (i)



        ec2filt = [{'Name': 'tag:string', 'Values': ['string']}]
        #ec2filt = [{'Name': 'tag:string', 'Values': ['string'],'Name': 'instance-state-name','Values': ['running']}]

        #ec2.instances.key_name = "bastion"

        click.pause('Чтобы продолжить, нажмите Enter.')

        ec2.instances.filter(Filters=ec2filt).terminate()



        #print(ec2.instances.filter(Filters=ec2filt).)

    except Exception as e:
        print(e)





create_ec2_instance()
