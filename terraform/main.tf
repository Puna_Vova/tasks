#----------------------------------------------------------
# My Terraform
#
# Use Our Terraform Module to create AWS VPC Networks
#
# Made by Denis Astahov. Summer 2019
#----------------------------------------------------------
provider "aws" {
  region = var.region
}


//vpc

module "vpc-test" {
  source = "../modules/VPC"
  // source               = "git@github.com:adv4000/terraform-modules.git//aws_network"
  env                  = "staging"
  vpc_cidr             = "10.10.0.0/16"
  public_subnet_cidrs  = ["10.10.1.0/24"]
  private_subnet_cidrs = ["10.10.11.0/24", "10.10.21.0/24"]
}


//S
/*
module "vote_service_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "user-service"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  //vpc_id      = "vpc-0cb21274355bdfb32"
  vpc_id = var.nn

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp"]

  ingress_with_cidr_blocks = [
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

}
*/


module "vote_service_sg" {
  source = "../modules/SG"
  vpcid  = module.vpc-test.vpc_id
}


module "vote_service_RDS" {
  source = "../modules/RDS"
  sub    = module.vpc-test.private_subnet_ids
}


/*
resource "aws_instance" "web" {
  ami                    = "ami-0574da719dca65348"
  instance_type          = "t3.micro"
  subnet_id              = module.vpc-test.public_subnet_ids[0]
  vpc_security_group_ids = module.vote_service_sg.id
  key_name               = "lab13"
  tags = {
    Name = "HelloWorld"
  }
}
*/

module "ec2_private" {
  source = "../modules/instance"
  sub    = module.vpc-test.private_subnet_ids[0]
  sg_id  = module.vote_service_sg.id
}

module "ec2_public" {
  source = "../modules/instance"
  sub    = module.vpc-test.public_subnet_ids[0]
  sg_id  = module.vote_service_sg.id
}
