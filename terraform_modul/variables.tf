variable "aws_region" {
    description = "AWS region"
    default     = "us-east-1"
}

variable "env" {
    description = "Name of environment"
    default     = "terraform"
}