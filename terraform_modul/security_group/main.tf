resource "aws_security_group" "security_22_80_443" {
  name        = "security_22_80_443"
  description = "security_22_80_443"
  vpc_id      = var.vpc_id.vpc_id
  ingress {
    description      = "open_80"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "open_443"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "open_22"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "security_22_80_443"
  }
}
resource "aws_security_group" "security_22_only" {
  name        = "security_22_only"
  description = "security_22_only"
  vpc_id      = var.vpc_id.vpc_id
  ingress {
    description      = "open_22"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "security_22_only"
  }
}
resource "aws_security_group" "security_5432_only" {
  name        = "security_5432_only"
  description = "security_5432_only"
  vpc_id      = var.vpc_id.vpc_id
  ingress {
    description      = "open_5432"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "security_5432_only"
  }
}