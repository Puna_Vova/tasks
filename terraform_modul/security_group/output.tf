output "security_22_80_443" {
  value = aws_security_group.security_22_80_443.id
}
output "security_22_only" {
  value = aws_security_group.security_22_only.id
}
output "security_5432_only" {
  value = aws_security_group.security_5432_only.id
}