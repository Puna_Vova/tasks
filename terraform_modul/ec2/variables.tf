
variable "instance_type" {
    default = "t2.nano"
}
variable "aws_key_name" {
    description = "AWS key"
    default     = "EC2key"
}

# output from /security_group
variable "security_22_80_443" {
}
variable "security_22_only" {
}

# output from /vpc
variable "public_subnet_id" {
}
variable "private_subnet_id" {
}
variable "public_az" {
}
variable "private_az" {
}

# from /
variable "env" {
}





