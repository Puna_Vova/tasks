data "aws_ami" "ubuntu_latest" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

resource "aws_instance" "bastion" {
  count = 1
  ami                    = data.aws_ami.ubuntu_latest.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [var.security_22_only]
  availability_zone      = element(var.public_az[*], count.index)
  key_name               = var.aws_key_name
  subnet_id              = element(var.public_subnet_id[*], count.index)
  tags = {
    Name = "${var.env}_bastion"
  }
}
resource "aws_instance" "private_server" {
  count = 1
  ami                    = data.aws_ami.ubuntu_latest.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [var.security_22_80_443]
  availability_zone      = var.private_az
  key_name               = var.aws_key_name
  subnet_id              = element(var.private_subnet_id[*], count.index)
  #subnet_id              = var.private_subnet_id
  tags = {
    Name = "${var.env}_private_server"
  }
}