output "public_ip_of_bastion" {
  value = aws_instance.bastion[*].public_ip
}
output "private_ip_of_bastion" {
  value = aws_instance.bastion[*].private_ip
}

output "ip_of_private_server" {
  value = aws_instance.private_server[*].private_ip
}

output "ubuntu_latest_ami_name" {
  value = data.aws_ami.ubuntu_latest.name
}

output "ubuntu_latest_ami_id" {
  value = data.aws_ami.ubuntu_latest.id
}