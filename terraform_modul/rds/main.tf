resource "aws_db_subnet_group" "terraform_postgre" {
  name       = "terraform_postgre"
  subnet_ids = [var.public_subnet_id[0], var.public_subnet_id[1]]
  tags = {
    Name = "terraform_postgre_group"
  }
}

resource "aws_db_instance" "dbpostgresrds" {
  allocated_storage          = 5
  identifier                 = "dbpostgresrds"
  db_name                    = "dbpostgresrds"
  engine                     = "postgres"
  engine_version             = "14.1"
  instance_class             = "db.t3.micro"
  username                   = "postgress"
  password                   = var.db_password
  db_subnet_group_name       = aws_db_subnet_group.terraform_postgre.name
  skip_final_snapshot        = true
  publicly_accessible        = true
  multi_az                   = false
  auto_minor_version_upgrade = false
  vpc_security_group_ids     = [var.security_5432_only]
  tags = {
    Name = "terraform-postgre-rds"
  }
}