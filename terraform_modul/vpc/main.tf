###### VPC -----------------------
resource "aws_vpc" "terraform" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = "true"
  tags = {
    Name = "${var.env}_vpc"
    }
}
###### Subnets -----------------------
resource "aws_subnet" "_public_subnet" {
  count                   = length(var.vpc_cidr_public)
  vpc_id                  = aws_vpc.terraform.id
  cidr_block              = element(var.vpc_cidr_public, count.index)
  availability_zone       = element(var.public_az, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.env}_public_subnet_${count.index + 1}"
  }
}
resource "aws_subnet" "_private_subnet" {
  count             = length(var.vpc_cidr_private)
  vpc_id            = aws_vpc.terraform.id
  cidr_block        = element(var.vpc_cidr_private, count.index)
  availability_zone = var.private_az
  tags = {
    Name = "${var.env}_private_subnet_${count.index + 1}"
  }
}
###### IGW -----------------------
resource "aws_internet_gateway" "_igw" {
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "${var.env}_igw"
  }
}
###### NAT + Elastic IP -----------
resource "aws_eip" "_elastic_ip" {
  count = length(var.vpc_cidr_private)
  vpc   = true
  #depends_on = [aws_internet_gateway._igw]
  tags = {
    Name = "${var.env}_nat_gtw.id_${count.index + 1}"
  }
}
resource "aws_nat_gateway" "_nat_gtw" {
  count         = length(var.vpc_cidr_private)
  allocation_id = aws_eip._elastic_ip[count.index].id
  subnet_id     = element(aws_subnet._public_subnet[*].id, count.index)
  #depends_on  = [aws_internet_gateway._igw]
  tags = { Name = "${var.env}_nat_gtw_${count.index + 1}"
  }
}
###### create public route ----------
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.terraform.id
  route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway._igw.id
  }
  tags = {
      Name = "${var.env}_public_route_table"
  }
}
resource "aws_route_table_association" "public_subnet_association" {
  count          = length(aws_subnet._public_subnet[*].id)
  subnet_id      = element(aws_subnet._public_subnet[*].id, count.index)
  route_table_id = aws_route_table.public_route_table.id
}
###### create private route -----------
resource "aws_route_table" "private_route_table" {
  count    = length(var.vpc_cidr_private)
  vpc_id   = aws_vpc.terraform.id
  route {
        cidr_block     = "0.0.0.0/0"
        nat_gateway_id = element(aws_nat_gateway._nat_gtw[*].id, count.index)
  }
  tags = {
        Name = "${var.env}_private_route_table_${count.index + 1}"
    }
}
resource "aws_route_table_association" "private_subnet_association" {
  count          = length(aws_subnet._private_subnet[*].id)
  subnet_id      = element(aws_subnet._private_subnet[*].id, count.index)
  route_table_id = aws_route_table.private_route_table[count.index].id
}