variable "public_az" {
    description = "Public zone"
    default     = ["us-east-1d", "us-east-1f"]
}
variable "private_az" {
    description = "Private zone"
    default     = "us-east-1e"
}
variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default     = "10.0.0.0/16"
}
variable "vpc_cidr_public" {
    description = "CIDR for the Public subnet"
    default     = ["10.0.1.0/24", "10.0.2.0/24"]
}
variable "vpc_cidr_private" {
    description = "CIDR for the Private subnet"
    default     = ["10.0.11.0/24"]
}

# from /
variable "env" {
}