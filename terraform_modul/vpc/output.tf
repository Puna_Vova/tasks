output "vpc_id" {
  value = aws_vpc.terraform.id
}
output "vpc_cidr" {
  value = aws_vpc.terraform.cidr_block
}
output "public_subnet_id" {
  value = aws_subnet._public_subnet[*].id
}
output "private_subnet_id" {
  value = aws_subnet._private_subnet[*].id
}
output "nat_gateway_id" {
  value = aws_eip._elastic_ip[*].id
}
output "vpc_cidr_public" {
  value = var.vpc_cidr_public
}
output "public_az" {
  value = var.public_az
}
output "private_az" {
  value = var.private_az
}