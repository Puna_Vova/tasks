provider "aws" {
    region = var.aws_region
}

module "vpc" {
  source = "./vpc"
  env = var.env
  #public_az      = module.vpc.public_az
  #private_az = module.vpc.private_az
  #vpc_cidr = module.vpc.vpc_cidr
  #vpc_cidr_public = module.vpc.vpc_cidr_public
  #vpc_cidr_private = module.vpc.vpc_cidr_private
}

output "vpc_module" {
  value = module.vpc
}

module "security_group" {
  source   = "./security_group"
  vpc_id   = module.vpc
  vpc_cidr = module.vpc
}
output "security_group_module" {
  value = module.security_group
}

module "ec2" {
  source             = "./ec2"
  env                = var.env
  public_az          = module.vpc.public_az
  private_az         = module.vpc.private_az
  private_subnet_id  = module.vpc.private_subnet_id
  public_subnet_id   = module.vpc.public_subnet_id
  security_22_80_443 = module.security_group.security_22_80_443
  security_22_only   = module.security_group.security_22_only
}
output "ec2_module" {
  value = module.ec2
}

module "rds" {
  source             = "./rds"
  public_subnet_id   = module.vpc.public_subnet_id[*]
  security_5432_only = module.security_group.security_5432_only
}
output "rds_module" {
  value     = module.rds
  sensitive = true
}